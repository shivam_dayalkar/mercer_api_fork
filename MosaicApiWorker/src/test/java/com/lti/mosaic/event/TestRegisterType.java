package com.lti.mosaic.event;

import java.io.IOException;

import org.junit.Test;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;

public class TestRegisterType {

	@Test
	public void test() throws IOException {
		String data="{\"EventMaster\":{\"EventName\":\"Async_progress\",\"EventDesc\":\"This event is used for saving state of async progress\",\"EventStr\":{\"request_id\":\"uuid_385343\", \"status\":\"3-Picked-up-for-processing\"},\"UpdatedBy\":\"Balkrushna Patil\" } }";
		registerEventType(data);
	}
	
	public void registerEventType(String data) throws IOException {
		String eventURL="https://dal-uat-apigee.mercer.com/api/v1/events/api/tracker/eventMaster";
		String apiKey = Cache.getProperty(CacheConstants.API_KEY);
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, data);
		Request request = new Request.Builder()
		  .url(eventURL)
		  .post(body)
		  .addHeader("apikey", apiKey)
		  .addHeader("content-type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response = client.newCall(request).execute();
		
		System.out.println(response.message());
	}



}
