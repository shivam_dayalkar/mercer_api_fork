package com.lti.mosaic.function.def;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.db.EBDbUtils;
import com.lti.mosaic.derby.DerbyConnection;
import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.parser.utils.ParamUtils;

public class LookUpFunctionBso {
private static final String FORMAT_CONSTANT = "{} {}";
private static final Logger logger = LoggerFactory.getLogger(LookUpFunctionBso.class);
  private static DerbyConnection jdbcInstance = DerbyConnection.getInstance();
  private static final String COMMAND = ",";
  private static Map<String,String> subQueryMap = new HashMap<>(); 

  private static final String PARSE_RESULT_SET = "LookUpFunctionBso - parseResultSet() :";

	public static String getSubquery(String clause, String table) {
		
		if (subQueryMap.containsKey(table + "|" + clause)) {
			return subQueryMap.get(table + "|" + clause);
		} else {
			StringBuffer subquery = new StringBuffer();
			StringBuffer result = new StringBuffer();
			Map<String, String> tableSchemaDataMap = null;

			tableSchemaDataMap = getTableSchema(table, tableSchemaDataMap);

			if (clause.trim().length() > 0) {
				subquery.append(StringEscapeUtils.unescapeJava(handleNullCasesInClause(clause)).replace("\"", "'"));
			}

			if (tableSchemaDataMap != null) {
				
				for (Map.Entry<String, String> entry : tableSchemaDataMap.entrySet()) {
					
					if (subquery.toString().contains(entry.getKey())) {
						findAndModifyInputByColumnDataType(subquery, result, entry);
					}
					
				}
			}

			if ("".equals(result.toString())) { // If no pattern matches, then use same old query (subquery)
				result.append(subquery);
			}
			String subQuery = " where " + result.toString();
			subQueryMap.put(table + "|" + clause, subQuery);
			return subQuery;
		}

	}

	/**
	 * @param subquery
	 * @param result
	 * @param entry
	 */
	private static void findAndModifyInputByColumnDataType(StringBuffer subquery, StringBuffer result,
			Map.Entry<String, String> entry) {
		if (!EBDbUtils.isStringDataType(entry.getValue())) {
			
			Pattern p = Pattern.compile("(" + entry.getKey()
					+ ")\\s*(=)(((\\s*)'(\\w|\\s|!|@|#|$|%|^|&|\\*|\\(|\\)|=|<|>|\\]|\\\\)*'(\\s*))*?)(AND|OR|&&|\\|\\||$)");
			Matcher m = p.matcher(subquery);
			
			while (m.find()) {
				String str = m.group(3).trim();
				if (str.startsWith("'")) {
					str = str.substring(1, str.length());
				}
				if (str.endsWith("'")) {
					str = str.substring(0, str.length() - 1);
				}
				m.appendReplacement(result, m.group(1) + m.group(2) + " " + str + " " + m.group(8));
			}
		}
	}

	/**
	 * @param table
	 * @param tableSchemaDataMap
	 * @return
	 */
	private static Map<String, String> getTableSchema(String table, Map<String, String> tableSchemaDataMap) {
		try {
			tableSchemaDataMap = EBDbUtils.getTableSchemaData(table);
		} catch (SQLException |IOException e) {
			logger.error("LookUpFunctionBso - getSubquery() : {}", e.getMessage());
		}
		return tableSchemaDataMap;
	}

  public static String getColumns(String arg3) {
    List<String> outputColumnNames = new ArrayList<>();
    outputColumnNames.addAll(Arrays.asList(arg3.split(COMMAND)));
    return StringUtils.join(outputColumnNames, COMMAND);
  }

  
  public static List<List<Object>> executeHbaseQuery(String query) {
    List<List<Object>> output = null;
    Connection conn = null;
    try {
      conn = MercerDBCP.getDataSource().getConnection();
      logger.debug("{}","Connection to MercerDB is opened...");
      output = jdbcInstance.get(conn, query);

    } catch (Exception e) {
      logger.error(e.getMessage());
      return output;
    } finally {
      if (null != conn) {
        try {
          conn.close();
        } catch (SQLException e) {
          logger.error(e.getMessage());
        }
      }
    }
    // If limit 1 Present
    return output;
  }

  public List<List<Object>> parseResultSet(ResultSet resultSet) {
    List<List<Object>> resultOutput = new ArrayList<>();
    try {
      logger.debug(FORMAT_CONSTANT, PARSE_RESULT_SET, resultSet);
      List<Object> columnFieldsName = new ArrayList<>();
      for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
        columnFieldsName.add(resultSet.getMetaData().getColumnName(i));
        logger.info("LookUpFunctionBso - parseResultSet(resultSet.getMetaData().getColumnName(i))"
            + ParamUtils.getString(resultSet.getMetaData().getColumnName(i)));
      }
      resultOutput.add(columnFieldsName);

      while (resultSet.next()) {
        List<Object> columnFieldsValues = new ArrayList<>();
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
          String record = resultSet.getObject(i) + "";
          if (StringUtils.isBlank(record) || StringUtils.equalsIgnoreCase(record, "null")) {
            columnFieldsValues.add(null);
          } else {
            columnFieldsValues.add(StringEscapeUtils.escapeJava(record));
          }
        }
        resultOutput.add(columnFieldsValues);
      }
    } catch (SQLException e) {
      logger.error(FORMAT_CONSTANT, PARSE_RESULT_SET, e.getMessage());
    }
    logger.debug(FORMAT_CONSTANT, PARSE_RESULT_SET, ParamUtils.getString(resultOutput));
    return resultOutput;
  }

  private static ArrayList<String> allSearchEqualPatterns = new ArrayList<>();
  private static ArrayList<String> allSearchNotEqualPatterns = new ArrayList<>();
  static {
    allSearchEqualPatterns.add("=null ");
    allSearchEqualPatterns.add("=NULL ");
    allSearchEqualPatterns.add("=Null ");
    allSearchEqualPatterns.add("= null ");
    allSearchEqualPatterns.add("= NULL ");
    allSearchEqualPatterns.add("= Null ");
    allSearchEqualPatterns.add("=  NULL ");
    allSearchEqualPatterns.add("=  null ");
    allSearchEqualPatterns.add("=  Null ");
    allSearchEqualPatterns.add("=null");
    allSearchEqualPatterns.add("=NULL");
    allSearchEqualPatterns.add("=Null");
    allSearchEqualPatterns.add("= null");
    allSearchEqualPatterns.add("= NULL");
    allSearchEqualPatterns.add("= Null");
    allSearchEqualPatterns.add("=  NULL");
    allSearchEqualPatterns.add("=  null");
    allSearchEqualPatterns.add("=  Null");

    //

    allSearchNotEqualPatterns.add("!=null ");
    allSearchNotEqualPatterns.add("!=NULL ");
    allSearchNotEqualPatterns.add("!=Null ");
    allSearchNotEqualPatterns.add("!= null ");
    allSearchNotEqualPatterns.add("!= NULL ");
    allSearchNotEqualPatterns.add("!= Null ");
    allSearchNotEqualPatterns.add("!=  NULL ");
    allSearchNotEqualPatterns.add("!=  null ");
    allSearchNotEqualPatterns.add("!=  Null ");
    allSearchNotEqualPatterns.add("!=null");
    allSearchNotEqualPatterns.add("!=NULL");
    allSearchNotEqualPatterns.add("!=Null");
    allSearchNotEqualPatterns.add("!= null");
    allSearchNotEqualPatterns.add("!= NULL");
    allSearchNotEqualPatterns.add("!= Null");
    allSearchNotEqualPatterns.add("!=  NULL");
    allSearchNotEqualPatterns.add("!=  null");
    allSearchNotEqualPatterns.add("!=  Null");
  }

  /**
   * This method will check for combination of "= null" and "!= null" since it is not supported by
   * derby Derby supports is null and is not null
   * 
   * @param clause
   * @return
   */
  public static String handleNullCasesInClause(String clause) {

    if (null != clause && clause.toLowerCase().contains("null")) {
      //Use Regex to replace "=null" to "is null"

      for (String searchPattern : allSearchEqualPatterns) {

        clause = clause.replaceAll(searchPattern, searchPattern.replace("=", " is "));

      }

      for (String searchPattern : allSearchNotEqualPatterns) {

        clause = clause.replaceAll(searchPattern, searchPattern.replace("!=", " is not "));

      }
    }

    return clause;

  }

}
