package in.lnt.validtion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author Sarang Gandhi
 *
 */
public class CustomAPIError extends Exception {

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatusCode; // status: the HTTP status code
	private String errorMessage; // message: the error message associated with exception
	private List<String> errorMessageList = new ArrayList<>(); // error: List of constructed error messages

	public CustomAPIError() {

	}

	/**
	 * 
	 * @param badRequest
	 * @param message
	 * @param errors
	 */
	public CustomAPIError(HttpStatus httpStatus, String message, List<String> errors) {
		super();
		httpStatusCode = httpStatus;
		errorMessage = message;
		errorMessageList = errors;
	}

	public CustomAPIError(HttpStatus httpStatus, String message, String error) {
		super();
		httpStatusCode = httpStatus;
		errorMessage = message;
		errorMessageList = Arrays.asList(error);
	}

	public CustomAPIError(HttpStatus httpStatus, String message) {
		httpStatusCode = httpStatus;
		errorMessage = message;

	}

	/**
	 * 
	 * @return http Status code
	 */
	public HttpStatus getHttpStatus() {
		return httpStatusCode;
	}

	/**
	 * @param httpStatusCode
	 *            the _httpStatus to set
	 */
	public void setHttpStatus(HttpStatus httpStatus) {
		httpStatusCode = httpStatus;
	}

	/**
	 * @return Error message
	 * 
	 */
	@Override
	public String getMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the _message to set
	 */
	public void setMessage(String message) {
		errorMessage = message;
	}

	/**
	 * @return the _errors
	 */
	public List<String> getErrors() {
		return errorMessageList;
	}

	/**
	 * @param errorMessageList
	 *            the _errors to set
	 */
	public void setErrors(List<String> errors) {
		errorMessageList = errors;
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
