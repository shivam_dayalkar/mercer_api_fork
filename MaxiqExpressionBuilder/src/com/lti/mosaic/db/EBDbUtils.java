package com.lti.mosaic.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.function.def.CustomFunctions;


public class EBDbUtils {
  private static final Logger logger = LoggerFactory.getLogger(EBDbUtils.class);

  private EBDbUtils() {
  }
  
  private static Map<String, Map<String, String>> tableSchemaDataCache =
      new HashMap<>();

  public static Map<String, String> getTableSchemaData(String tableName)
      throws SQLException, IOException {

    if (tableSchemaDataCache.containsKey(tableName)) {
      return tableSchemaDataCache.get(tableName);
    } else {

      Map<String, String> metaMap = new HashMap<>();

      // Result Set MetaData Object
      ResultSetMetaData rsmd = null;
      // Result Set Object
      ResultSet resultSet = null;

      long startTime = 0;

      StringBuilder query = new StringBuilder(" SELECT * FROM ");
      query.append(tableName).append(" ").append(CustomFunctions.FIRST_ROW);
      
      try ( // DB Connection Object
			Connection dbConnection = MercerDBCP.getDataSource().getConnection();
			// Using query base approach
			Statement statementObj = dbConnection.createStatement();) {
        // Derby Start time
        startTime = System.currentTimeMillis();
        
        resultSet = statementObj.executeQuery(query.toString());
        rsmd = resultSet.getMetaData();

        for (int j = 1; j <= rsmd.getColumnCount(); j++) {
          getActualDataType(metaMap, rsmd, j);
        }
        logger.debug(
            "Time taken for creating connection and getting result statement for getRefTableMetaData() "
                + "in DBUtiils..",
            (System.currentTimeMillis() - startTime));

      } catch (SQLException ex) {
        logger.error("Exception occured in getRefTableMetaData.. {}", ex.getMessage());
        throw ex;
      } finally {
        if (null != resultSet) {
          resultSet.close();
        }
        if (null != rsmd) {
          rsmd = null;
        }
      }
      tableSchemaDataCache.put(tableName, metaMap);
      return metaMap;
    }
  }

  public static void getActualDataType(Map<String, String> metaMap, ResultSetMetaData rsmd, int j)
      throws SQLException {
    switch (rsmd.getColumnType(j)) {
      case java.sql.Types.BIT:
          metaMap.put(rsmd.getColumnName(j), "BIT");
          break;
      case java.sql.Types.TINYINT:
          metaMap.put(rsmd.getColumnName(j), "TINYINT");
          break;
      case java.sql.Types.INTEGER:
          metaMap.put(rsmd.getColumnName(j), "INTEGER");
          break;    
      case java.sql.Types.BIGINT:
    	  metaMap.put(rsmd.getColumnName(j), "BIGINT");
    	  break;
      case java.sql.Types.FLOAT:
    	  metaMap.put(rsmd.getColumnName(j), "FLOAT");
    	  break;
      case java.sql.Types.REAL:
    	  metaMap.put(rsmd.getColumnName(j), "REAL");
    	  break;
      case java.sql.Types.DOUBLE:
    	  metaMap.put(rsmd.getColumnName(j), "DOUBLE");
    	  break;
      case java.sql.Types.NUMERIC:
    	  metaMap.put(rsmd.getColumnName(j), "NUMERIC");
    	  break;
      case java.sql.Types.DECIMAL:
    	  metaMap.put(rsmd.getColumnName(j), "DECIMAL");
    	  break;
      case java.sql.Types.CHAR:
    	  metaMap.put(rsmd.getColumnName(j), "CHAR");
    	  break;
      case java.sql.Types.VARCHAR:
    	  metaMap.put(rsmd.getColumnName(j), "VARCHAR");
    	  break;
      case java.sql.Types.LONGVARCHAR:
    	  metaMap.put(rsmd.getColumnName(j), "LONGVARCHAR");
    	  break;
      case java.sql.Types.DATE:
    	  metaMap.put(rsmd.getColumnName(j), "DATE");
    	  break;
      case java.sql.Types.TIME:
    	  metaMap.put(rsmd.getColumnName(j), "TIME");
    	  break;
      case java.sql.Types.TIMESTAMP:
    	  metaMap.put(rsmd.getColumnName(j), "TIMESTAMP");
    	  break;
      case java.sql.Types.BINARY:
    	  metaMap.put(rsmd.getColumnName(j), "BINARY");
    	  break;
      case java.sql.Types.VARBINARY:
    	  metaMap.put(rsmd.getColumnName(j), "VARBINARY");
    	  break;
      case java.sql.Types.LONGVARBINARY:
    	  metaMap.put(rsmd.getColumnName(j), "LONGVARBINARY");
    	  break;
      case java.sql.Types.NULL:
    	  metaMap.put(rsmd.getColumnName(j), "NULL");
    	  break;
      case java.sql.Types.OTHER:
    	  metaMap.put(rsmd.getColumnName(j), "OTHER");
    	  break;
      case java.sql.Types.JAVA_OBJECT:
    	  metaMap.put(rsmd.getColumnName(j), "JAVA_OBJECT");
    	  break;
      case java.sql.Types.DISTINCT:
    	  metaMap.put(rsmd.getColumnName(j), "DISTINCT");
    	  break;
      case java.sql.Types.STRUCT:
    	  metaMap.put(rsmd.getColumnName(j), "STRUCT");
    	  break;
      case java.sql.Types.ARRAY:
    	  metaMap.put(rsmd.getColumnName(j), "ARRAY");
    	  break;
      case java.sql.Types.BLOB:
    	  metaMap.put(rsmd.getColumnName(j), "BLOB");
    	  break;
      case java.sql.Types.CLOB:
    	  metaMap.put(rsmd.getColumnName(j), "CLOB");
    	  break;
      case java.sql.Types.REF:
    	  metaMap.put(rsmd.getColumnName(j), "REF");
    	  break;
      default :
    	  break;
    }
  }

  public static boolean isStringDataType(String unKnownDataType) {

    boolean isString = true;
    switch (unKnownDataType) {
      case "BIT":
      case "TINYINT":
      case "INTEGER":
      case "BIGINT":
      case "FLOAT":
      case "REAL":
      case "DOUBLE":
      case "NUMERIC":
      case "DECIMAL":
      case "BINARY":
      case "BLOB":
      case "CLOB":
      case "NULL":
    	   isString = false;
    	   break;
      case "CHAR":
      case "VARCHAR":
      case "LONGVARCHAR":
      case "DATE":
      case "TIME":
      case "TIMESTAMP":
    	   break;
      default:
    	  break;
    }
    return isString;
  }

}
