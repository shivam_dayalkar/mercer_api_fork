package com.lti.mosaic.parser.constants;

public class LoggerConstants {
	
	private LoggerConstants() {
	}

	public static final String LOG_MERCERWEB = "LOG_MERCERWEB";
	public static final String LOG_MAXIQAPI = "MOSAIC_API";
}
