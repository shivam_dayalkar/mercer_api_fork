package com.lti.mosaic.derby;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.parser.constants.LoggerConstants;

/**
 * @author rushikesh
 *
 */
public class DerbyUtils {
  
  private static final Logger logger = LoggerFactory.getLogger(DerbyUtils.class);
  
  private DerbyUtils() {
  }

  /**
   * @param columnTypeName
   * @return
   */
  public static String convertColumnType(String columnTypeName) {

    if (StringUtils.equalsIgnoreCase(columnTypeName, "VARCHAR")) {
      return "VARCHAR(1000)";
    }
    return columnTypeName;
  }

 
  /**
   * 
   * @param mysqlConn
   * @return
   * @throws Exception
   */
  public static List<String> getAllTables(Connection mysqlConn) throws Exception {

    DatabaseMetaData dbMd = null;
    ResultSet resultSet = null;

    ArrayList<String> tableNames = new ArrayList<>(50);
    // Using sql connection get Meta Data for all tables in Database
    dbMd = mysqlConn.getMetaData();
    // Get tables listing in the resultset.
    resultSet = dbMd.getTables(null, null, "%", null);

    while (resultSet.next()) {
      String tableName = resultSet.getString(3);
      // Is this checking lower case input for Table Name ??
      if(tableName.equals(tableName.toLowerCase())){
        tableNames.add(tableName);
      } else {
				logger.error("{} ignoring {}", LoggerConstants.LOG_MAXIQAPI, tableName);
      }
    }
    logger.info("Table Name = {}", tableNames);

    resultSet.close();
    return tableNames;
  }
  
  /**
   * @param tableName
   * @param mysqlMetaData
   * @throws SQLException
   */
  public static String generateCreateStatement(String tableName, ResultSetMetaData mysqlMetaData)
			throws SQLException {

		StringBuilder createStatement = new StringBuilder(" CREATE TABLE ").append(tableName).append(" ( ");

		for (int columnIndex = 1; columnIndex <= mysqlMetaData.getColumnCount(); columnIndex++) {
			String columnType = convertColumnType(mysqlMetaData.getColumnTypeName(columnIndex));

			createStatement.append(" \"").append(mysqlMetaData.getColumnName(columnIndex).toUpperCase()).append("\" ");
			createStatement.append(columnType).append(" , ");
		}

	    createStatement.replace(createStatement.length() - 2, createStatement.length(), "");
	    createStatement.append(")");

		return createStatement.toString();
	}
  
  /**
   * @param derbyConnection
   * @param mysqlConn
   * @param tableName
   * @throws SQLException
   */
  public static void insertDataIntoDerbyTable(Connection derbyConnection, Connection mysqlConn,
      String tableName) throws SQLException {
    
    logger.info("{} Started derby table for {}",LoggerConstants.LOG_MAXIQAPI, tableName);
    Long startTime = System.currentTimeMillis();
    
	StringBuilder selectStatement = new StringBuilder(" select * from ").append(tableName);

		try (PreparedStatement mysqlSelectStatement = mysqlConn.prepareStatement(selectStatement.toString());
				ResultSet mysqlResultSet = mysqlSelectStatement.executeQuery();
				Statement statement = derbyConnection.createStatement();) {

    ResultSetMetaData mysqlMetaData = mysqlResultSet.getMetaData();

    String schema = DerbyUtils.generateCreateStatement(tableName, mysqlMetaData);

    statement.execute(schema);
    
    //Double quotes are necessary because derby is not case sensitive(removed this case now)
	StringBuilder insertStatement = new StringBuilder(" insert into ").append(tableName).append(" values (");

    for (int coulmnIndex = 0; coulmnIndex < mysqlResultSet.getMetaData()
        .getColumnCount(); coulmnIndex++) {
      insertStatement.append(" ? ,");
    }
    insertStatement.replace(insertStatement.length() - 2, insertStatement.length(), "");
    insertStatement.append(")");
    
    
    while (mysqlResultSet.next()) {
		try (PreparedStatement derbyInsertStatement = derbyConnection.prepareStatement(insertStatement.toString());) {
			for (int i = 1; i <= mysqlResultSet.getMetaData().getColumnCount(); i++) {

				derbyInsertStatement.setObject(i, mysqlResultSet.getObject(i));

			}
			derbyInsertStatement.executeUpdate();
		}
    }
    
    mysqlResultSet.last();
    Integer size =  mysqlResultSet.getRow();
    
    String generateIndexCreateStatement = generateIndexCreateStatement(tableName, mysqlMetaData);
    try(Statement derbyStatement = derbyConnection.createStatement();){
    	derbyStatement.execute(generateIndexCreateStatement);
    }

	logger.info("{} << Derby Loaded {}. Time - {}. Size - {}", LoggerConstants.LOG_MAXIQAPI, tableName,
					(System.currentTimeMillis() - startTime), size);
	}
  }
  
  /**
   * @param tableName
   * @param mysqlMetaData
   * @throws SQLException
   */
  public static String generateIndexCreateStatement(String tableName, ResultSetMetaData mysqlMetaData)
      throws SQLException {
	  
	StringBuilder createStatement = new StringBuilder(" CREATE Index ").append(tableName).append("_ind on ").append(tableName).append(" ( ");

    for (int columnIndex = 1; columnIndex <= 2; columnIndex++) {
    	createStatement.append(" \"").append(mysqlMetaData.getColumnName(columnIndex).toUpperCase()).append("\" , ");
    }

    createStatement.replace(createStatement.length() - 2, createStatement.length(), "");
    createStatement.append(")");
    return createStatement.toString();
  }

}
