package com.lti.mosaic.api.worker.callback;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import java.util.Base64;

/**
 * 
 * @author aavesh
 *
 */
public class CallBackManager {

  private CallBackManager() {
	  //default constructor
  }
  private static final Logger LOGGER = LoggerFactory.getLogger(CallBackManager.class);
  private static final Integer MAXRETRYCALLBACK=
      Integer.valueOf(Cache.getProperty(CacheConstants.MAX_RETRY_CALL_BACK));
  private static final Integer WAITTIMEBETWEENCALLBACK=
      Integer.valueOf(Cache.getProperty(CacheConstants.WAIT_TIME_BETN_CALL_BACK));

  /**
   * 
   * @param inputJson
   * @param urlString
   * @param requestType
   * @param checkCode
   * @param userName
   * @param password
   * @return
   * @throws Exception
   */
  public static HttpURLConnection makeHttpConnectionCallToRequest(String inputJson,
      String urlString, String requestType, String userName, String password, String apiKey)
      throws ConnectException {


    LOGGER.info("calling >> makeHttpConnectionCallToRequest"
        + ParamUtils.getString(inputJson, urlString, requestType));

    URL url;
    try {

      url = new URL(urlString);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      conn.setDoOutput(true);
      conn.setRequestMethod(requestType);
      conn.setRequestProperty(RequestConstants.CONTENT_TYPE, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty(RequestConstants.ACCEPT, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty("apikey", apiKey);

      if (null != userName || null != password) {

        String authString = userName + ":" + password;

        String encoding = Base64.getEncoder().encodeToString(authString.getBytes());

        conn.setRequestProperty(RequestConstants.AUTH, RequestConstants.BASIC_AUTH + encoding);
      }


      if (inputJson != null && !inputJson.isEmpty()) {
        OutputStream os = conn.getOutputStream();
        os.write(inputJson.getBytes());
        os.flush();
      }
      return conn;
    } catch (Exception e) {
      String loggerMsg = LoggerConstants.LOG_MAXIQAPI + CacheConstants.HTTP_CONNECTIONCALL_TO_REQUEST;
      LOGGER.error(loggerMsg, e.getMessage());
      throw new ConnectException(e.getMessage());
    }
  }

  /**
   * 
   * @param bufferedReader
   * @return
   * @throws IOException
   */
  private static StringBuilder generateResponeStream(BufferedReader br) throws IOException {

    String output = "";
    StringBuilder outputString = new StringBuilder();
    while ((output = br.readLine()) != null) {
      outputString.append(output);
    }
    //LOGGER.debug("The Web Response is {}", outputString);
    return outputString;
  }

  /**
   * 
   * @param inputJson
   * @param urlString
   * @param requestType
 * @throws SystemException 
 * @throws InterruptedException 
 * @throws IOException 
 */
  public static void sendResponseViaCallBackUrl(String inputJson, String urlString,
      String requestType, String apiKey) throws IOException, InterruptedException, SystemException {

    int retryNo = 1;
    HttpURLConnection connection =
        makeHttpConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);

    if (connection.getResponseCode() == 500) {
      connection.disconnect();
      
      LOGGER.debug("{}", CacheConstants.HTTP_CONNECTIONCALL_TO_REQUEST);

      while (retryNo <= MAXRETRYCALLBACK && connection.getResponseCode() == 500) {

        Thread.sleep(WAITTIMEBETWEENCALLBACK);

        connection =
            makeHttpConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);
        retryNo++;

      }

      if (connection.getResponseCode() != 500) {
        handleResponseNot500(connection);

      } else {
        LOGGER.error("Error while call back{}", connection.getResponseCode());
        connection.disconnect();
        ExceptionsMessanger.throwException(new SystemException(), "ERR_008", null);
      }

    } else {
      handleResponseNot500(connection);
    }
  }
  
  /**
   * 
   * @param connection
 * @throws SystemException 
 * @throws IOException 
   * @throws Exception if response is not 200
   */
  private static void handleResponseNot500(HttpURLConnection connection) throws IOException, SystemException {

    if (connection.getResponseCode() == 400 || connection.getResponseCode() == 200) {
    	generateResponseStream(connection);
    	if (connection.getResponseCode() == 400){
    		LOGGER.error("Status 400. Dead-letter{}", connection.getResponseCode());
    		ExceptionsMessanger.throwException(new SystemException(), "ERR_007", null);
    	}
    } else {
    	LOGGER.error("Unknown Status {}",connection.getResponseCode());
    	throw new SystemException("Unknown response code from call Back");
    }

  }

  private static void generateResponseStream(HttpURLConnection connection) throws IOException {

	  BufferedReader	br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
	  StringBuilder generateResponeStream = generateResponeStream(br);

	  connection.disconnect();
	  StringBuilder s= new StringBuilder();
	  s.append(LoggerConstants.LOG_MAXIQAPI).append(CacheConstants.HTTP_CONNECTIONCALL_TO_REQUEST).append(ParamUtils.getString(generateResponeStream));
	  LOGGER.debug("{}", s);
  }

  /**
   * 
   * @param requestId
   * @param documentId
   * @param status
   * @return
   */
  public static String createCallBackJson(String requestId, String documentId, Integer status, String message) {

    Map<String, Object> jsonMap = new HashMap<>();
    jsonMap.put(Constants.Request.REQUESTID, requestId);
    jsonMap.put(Constants.Request.DOCUMENTID, documentId);
    jsonMap.put(Constants.Request.STATUS, status);
    jsonMap.put(Constants.Request.MESSAGE, message);

    return ObjectSerializationHandler.toString(jsonMap);
  }

  /**
   * @param string
   * @return
   * @throws SystemException
   */
  public static String getCallBackUrlFromEnvironmentName(String string) throws SystemException {

    String callBackUrl = Cache.getProperty(string);

    if (null == callBackUrl) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_009", null);
    }
    return callBackUrl;
  }

  /**
   * @param string
   * @return
   * @throws SystemException
   */
  public static String getCallBackApiKeyFromEnvironmentName(String string) throws SystemException {

    String callBackApiKey = Cache.getProperty(string + "_API_KEY");

    if (null == callBackApiKey) {
      throw new SystemException("Cannot find call back apiKey");
    }
    return callBackApiKey;
  }

}