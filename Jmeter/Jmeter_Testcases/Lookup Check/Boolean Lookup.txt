{  
   "entities":[  
      {  
         "contextData":{  
            "campaignId":"59ca2a66c1e15a565af88407",
            "companyId":"59c3eac0fc22fa6928d09aa4",
            "companyName":"Exadel",
            "sectionId":"pe3707Section",
            "grpCode":"",
            "cpyCode":"",
            "ctryCode":"sayalu",
            "orgSize":{  
               "59c3eac0fc22fa6928d09aa4":null
            },
            "uniqueIdColumnCode":"YOUR_EEID",
            "industry":{  
               "superSector":"CG",
               "sector":"101",
               "subSector":"2670"
            }
         },
         "sections":[  
            {  
               "sectionCode":"incumbent_data",
               "sectionStructure":{  
                  "columns":[  
                     {  
                        "code":"question1",
                        "displayLabel":"Question 1 display label",
                        "dataType":"int",
                        "questionType":"integer",
                        "validations":[  
                           {
                              "message":"",
                              "expression":"",
                              "validationType":"sequentialCheck",
                              "dependentColumns": [],  
                              "_id":"59df5c44527b5be1ac230224",
                              "category":"input_data"
                           },
						   {
							"parameter": "",
							"expression": "BOOL_IN_LOOKUP(\"COUNTRY\",\"CTX_CTRY_CODE = contextData.ctryCode\",\"AGE_HIRE_MIN\")\n;",
							"errorType": "AUTOCORRECT",
							"message": "LOOKUP CHECK EXECUTED SUCESSFULLY",
							"validationType": "expression",
							"_id": "5a04224e23212300164a48aa",
							"category": "input_data"
							
							},
                           
						   { 
                           	  "mda":"clear_value",
                              "message":"",
                              "expression":"",
                              "validationType":"range",
                              "minError":"15000",
                              "maxError":"24500",
                              "minAlert":"17000",
                              "maxAlert":"21000",
                              "_id":"59df5c44527b5be1ac230224",
                              "category":"input_data",
                              "dependentColumns":[  

                              ]
                           },{  
                        "errorType":"ERROR",
                        "validationType":"expression",
                        "expression":"DECODE(LENGTH(this.question1) > 0,\"true\",\"false\");",
                        "errorGroup":"Personal",
                        "message":"Length of Department"
                    	   }
                        ]
                     },
                     {  
                        "code":"question2",
                        "displayLabel":"Question 2 display label",
                        "dataType":"int",
                        "questionType":"integer",
                        "validations":[  
                        ]
                     },
                     {  
                        "code":"question3",
                        "displayLabel":"Question 2 display label",
                        "dataType":"int",
                        "questionType":"integer",
                        "validations":[  
                        ]
                     },
                     {  
                        "code":"question4",
                        "displayLabel":"Question 2 display label",
                        "dataType":"int",
                        "questionType":"integer",
                        "validations":[  
                        ]
                     }
                  ]
               },
               "data":[  
                  {  
                     "$$id":"58d0360baa2ae02cc4723431",
                     "YOUR_EEID":"1",
                     "question1":"15000",
                     "question2":"20000",
					 "question3":"2000",
					 "question4":"8000",
                     "EMP006":"EMP_006"
                  },
                  {  
                     "$$id":"58d03653aa2ae02cc4723432",
                     "YOUR_EEID":"2",
                     "question1":"18000",
                     "question2":"20000",
					 "question3":"30000",
					 "question4":"80000",
                     "EMP006":"EMP_006"
                  }
               ]
            }
         ]
      }
   ]
}