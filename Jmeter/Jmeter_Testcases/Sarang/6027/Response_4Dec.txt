{
	"entities": [{
		"data": [{
			"city": "200",
			"YOUR_EEID": "133456789",
			"EMP006": "102",
			"COUNTRY_CODE": "200",
			"validationErrors": [{
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of City",
				"field": "city",
				"messageKey": "mosaic-error-message.max_value_error"
			}, {
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of Country",
				"field": "COUNTRY_CODE",
				"messageKey": "mosaic-error-message.max_value_error"
			}]
		}, {
			"city": "20",
			"YOUR_EEID": "143456789",
			"EMP006": "103",
			"COUNTRY_CODE": "100",
			"validationErrors": [{
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of City",
				"field": "city",
				"messageKey": "mosaic-error-message.min_value_error"
			}, {
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of Country",
				"field": "COUNTRY_CODE",
				"messageKey": "mosaic-error-message.min_value_error"
			}]
		}, {
			"city": "400",
			"YOUR_EEID": "153456789",
			"EMP006": "104",
			"COUNTRY_CODE": "50",
			"validationErrors": [{
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of City",
				"field": "city",
				"messageKey": "mosaic-error-message.max_value_error"
			}, {
				"errorGroup": "Personal",
				"errorType": "ERROR",
				"referenceTableName": "ref_table_name",
				"validationType": "rangeValidationRefTable",
				"message": "Length of Country",
				"field": "COUNTRY_CODE",
				"messageKey": "mosaic-error-message.min_value_error"
			}]
		}],
		"contextData": {
			"uniqueIdColumnCode": "ID",
			"campaignId": "11111",
			"grpCode": "1435",
			"companyName": "Company1",
			"cpyCode": "4444",
			"industry": {
				"superSector": "CG",
				"subSector": "101",
				"sector": "100"
			},
			"sectionId": "11111",
			"orgSize": 123,
			"CTX_CTRY_CODE": "US",
			"ctryCode": "PL"
		}
	}]
}