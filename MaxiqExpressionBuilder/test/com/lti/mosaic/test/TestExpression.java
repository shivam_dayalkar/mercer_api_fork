package com.lti.mosaic.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import com.lti.mosaic.antlr4.parser.ExpressionLexer;
import com.lti.mosaic.antlr4.parser.ExpressionParser;
import com.lti.mosaic.function.def.Value;
import com.lti.mosaic.parser.ErrorListener;
import com.lti.mosaic.parser.EvalVisitor;


public class TestExpression {

	public static void main(String[] args) throws IOException {
		System.out.println("MMM");
		// Set CSV file
		//File initialFile = new File("C:\\Users\\10639498\\Downloads\\result.csv");
		//File initialFile = new File("C:\\Users\\10639498\\Downloads\\TEstInversion.csv");
		//File initialFile = new File("C:\\Users\\10639498\\Downloads\\Meteorite_Landings_small.csv");
		//File initialFile = new File("C:\\Users\\10639498\\Downloads\\convertcsv (1).csv");
 
	//	System.out.println("LENGTH : " +  myArr.length());
	//	System.out.println("rowToString : " + CDL.rowToString(myArr));
	
		
		/*System.out.println();
		Double  d1 = 1.0;
		Integer d2 = 1;
		System.out.println("RES :" + (Math.exp(d1)));
		InputStream targetStream = new FileInputStream(initialFile);
		CsvParserSettings parserSettings = new CsvParserSettings();
		parserSettings.setLineSeparatorDetectionEnabled(true);
		RowListProcessor rowProcessor = new RowListProcessor();
		parserSettings.setRowProcessor(rowProcessor);
		parserSettings.setHeaderExtractionEnabled(true);
		CsvParser csvParser = new CsvParser(parserSettings);
		csvParser.parse(targetStream);
		String[] headers = rowProcessor.getHeaders();
		ArrayList<String[]> data = (ArrayList<String[]>) rowProcessor.getRows();
		data.add(0, headers);
		System.out.println("Data Size: " + data.size());*/
		EvalVisitor visitor = new EvalVisitor();
		boolean uuid = visitor.setCsvData(null,null,null,null,null);

		//ExpressionLexer lexer = new ExpressionLexer(new ANTLRFileStream("D:\\PROJECT\\expressions.txt"));
 		System.out.println("TEST");
		ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream("1.56789 != 1.56789;"));
 		//ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream("ANN_BASE = 203239;EMP_309 = 20;x = \"\"; IF (!ISBLANK(ANN_BASE) AND !ISBLANK(EMP_309)) {x = ANN_BASE < 203240 AND  EMP_309 < (ANN_BASE * 0.093);} ELSE {x = false;} x; "));
		//ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream(" TO_DATE(\"2017-21-10\",\"yyyy-dd-MM\"); "));
 
		lexer.removeErrorListeners();
		//lexer.addErrorListener(ErrorListener.INSTANCE);
		//ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream("LOOK_UP_CHECK ( \"ref_countries_csv\" , \"CTRY_CODE = 'AF' \" , \"CTRY_NAME\" )"));

 		//ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream(" IF(XMIN(\"mass\",\"  mass      >=    100\" , \"Fall=Fell\") > 74){ n = \"Hello\";}ELSE{ n = \"Bye\";}n;"));
		ExpressionParser parser = new ExpressionParser(new CommonTokenStream(lexer));
		parser.removeErrorListeners();
		//parser.addErrorListener(ErrorListener.INSTANCE);
		System.out.println(3 / 2);
		ParseTree tree = parser.parse();
 
		
		Value value = visitor.visit(tree);
		System.out.println("Result : " + value);
		printTree(Arrays.asList(parser.getRuleNames()), tree);
	}

	public static void printTree(List<String> list, ParseTree tree) {
		// begin parsing at rule 'r'
		JFrame frame = new JFrame("Antlr AST");
		JPanel panel = new JPanel();
		TreeViewer viewr = new TreeViewer(list, tree);
		viewr.setScale(1.5);// scale a little
		panel.add(viewr);
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(200, 200);
		frame.setVisible(true);

	}
}
