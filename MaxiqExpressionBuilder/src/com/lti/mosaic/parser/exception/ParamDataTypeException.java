package com.lti.mosaic.parser.exception;


/**
 * Licensed Information -- need to work on this 
 */

/**
 * Expception - In case of param data type is mismatch 
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class ParamDataTypeException extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public ParamDataTypeException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ParamDataTypeException(String message) {
		super(message);
	}
}
