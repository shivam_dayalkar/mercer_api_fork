Note:

POS_CODE					|	MEDIAN
ABC.03.002.M20 : 99, 98		|	98.5
ABC.03.002.S10 : 199		|	199
HRM.03.002.M20 : 10*10, 101	|	100.5
HRM.03.002.S10 : 201		|	201
HRM.03.002.P60 : 48			|	48
XYZ.03.002.P60 : 52			|	52
XYZ.03.002.E02 : 40			| 	40

RULES :
M20 > S10
M20 < P60
S10 <= P60
P60 < E02

Precedence of M20, S10, P60 to be chkd in DB

ERRORS : 
1. HRM.03.02. : 
		M20 - S10
		S10 - P60
		M20 - P60

2. XYZ.03.002. :
		P60 - E02
	
3. ABC.03.002. : 
		M20 - S10

EMP_129 : ANNUAL_BASE
EMP_125 : MONTH_BASE
EMP_128 : NUM_MONTHS
	
	
	1. ANNUAL_BASE
	2. month_base * (num_months)-- UK, US, PL,IN  (default set to 12 in DB) for any other country default value is set to 12
	
POSCODE:
AAA : Family
BB  : SubFamily
CCC : Speciality
D   : Career Stream
EE  : Career Level
	
Request : 
####################################################################################################
{
  "entities": [
    {
      "data": [
        {
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.M20",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "102",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.M20",
          "EMP_129": "101"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "103",
          "POS_CLASS": "55",
          "POS_CODE": "ABC.03.002.M20",
          "EMP_129": "99"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "104",
          "POS_CLASS": "55",
          "POS_CODE": "ABC.03.002.M20",
          "EMP_129": "98"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "105",
          "POS_CLASS": "52",
          "POS_CODE": "ABC.03.002.S10",
          "EMP_129": "199"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "106",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.S10",
          "EMP_129": "201"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "107",
          "POS_CLASS": "52",
          "POS_CODE": "XYZ.03.002.P60",
          "EMP_129": "52"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "108",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.P60",
          "EMP_129": "48"
        },
        {
          "EMP_125": "32000",
          "YOUR_EEID": "109",
          "POS_CLASS": "52",
          "POS_CODE": "XYZ.03.002.E02",
          "EMP_129": "40"
        }
      ],
      "sectionStructure": {
        "columns": [
          {
            "code": "EMP_125",
            "displayLabel": "Monthly Base Salary (Equivalent to 100% work time)",
            "dataType": "int",
            "questionType": "integer",
            "validations": [
              {
                "errorType": "ERROR",
                "validationType": "expression",
                "expression": "INVERSIONCHECK(XAVG(\"EMP_125\", \"EMP_125 >= 10\" , \"POS_CLASS\"));",
                "errorGroup": "Personal",
                "message": "Length of Department"
              }
            ]
          }
        ]
      },
      "otherSectionsData": {
        
      },
      "contextData": {
        "campaignId": "599820d4747cf240d90c0b8c",
        "companyId": "599820d4747cf240d90c0b8d",
        "sectionId": "incumbent_data",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "US",
        "orgSize": 20,
        "uniqueIdColumnCode": "YOUR_EEID",
        "industry": {
          "superSector": "HT",
          "sector": "196",
          "subSector": "2070"
        }
      }
    }
  ]
}

##########################################################################################################################################

Response:

{
    "entities": [
        {
            "validationResults": [
                {
                    "expression": "INVERSIONCHECK(XAVG(\"EMP_125\", \"EMP_125 >= 10\" , \"POS_CLASS\"));",
                    "errorGroup": "Personal",
                    "errorType": "ERROR",
                    "validationType": "expression",
                    "message": "Length of Department",
                    "field": "EMP_125",
                    "data": [
                        {
                            "POS_CLASS": "52",
                            "EMP_125": 27430
                        },
                        {
                            "POS_CLASS": "55",
                            "EMP_125": 32000
                        }
                    ]
                },
                {
                    "errorGroup": "Personal",
                    "errorType": "ERROR",
                    "validationType": "expression",
                    "message": "Job Inversion Errors",
                    "aggregateDisplayLabel": "Median Pay",
                    "field": "ANNUAL_BASE",
                    "category": "job_inversion",
                    "data": [
                        {
                            "POS_CODE": "ABC.03.002.S10",
                            "MEDIAN": 199,
                            "COUNT": 1,
                            "LINKED": [
                                "ABC.03.002.M20"
                            ]
                        },
                        {
                            "POS_CODE": "HRM.03.002.S10",
                            "MEDIAN": 201,
                            "COUNT": 1,
                            "LINKED": [
                                "HRM.03.002.M20",
                                "HRM.03.002.P60"
                            ]
                        },
                        {
                            "POS_CODE": "ABC.03.002.M20",
                            "MEDIAN": 98.5,
                            "COUNT": 2,
                            "LINKED": []
                        },
                        {
                            "POS_CODE": "XYZ.03.002.P60",
                            "MEDIAN": 52,
                            "COUNT": 1,
                            "LINKED": [
                                "XYZ.03.002.E02"
                            ]
                        },
                        {
                            "POS_CODE": "HRM.03.002.M20",
                            "MEDIAN": 100.5,
                            "COUNT": 2,
                            "LINKED": [
                                "HRM.03.002.P60"
                            ]
                        },
                        {
                            "POS_CODE": "HRM.03.002.P60",
                            "MEDIAN": 48,
                            "COUNT": 1,
                            "LINKED": []
                        },
                        {
                            "POS_CODE": "XYZ.03.002.E02",
                            "MEDIAN": 40,
                            "COUNT": 1,
                            "LINKED": []
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "YOUR_EEID",
                "companyId": "599820d4747cf240d90c0b8d",
                "campaignId": "599820d4747cf240d90c0b8c",
                "grpCode": "",
                "cpyCode": "",
                "industry": {
                    "superSector": "HT",
                    "subSector": "2070",
                    "sector": "196"
                },
                "sectionId": "incumbent_data",
                "orgSize": 20,
                "ctryCode": "US"
            }
        }
    ]
}
##########################################################################################################################################
