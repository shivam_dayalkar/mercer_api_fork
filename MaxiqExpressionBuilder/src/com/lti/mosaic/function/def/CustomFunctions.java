package com.lti.mosaic.function.def;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomFunctions {

	private static final Logger logger = LoggerFactory.getLogger(CustomFunctions.class);
	
	public static final String FIRST_ROW = " FETCH FIRST ROW ONLY ";
	private static final String FROM = " FROM ";
	private static final String SELECT = " SELECT ";
		
	private CustomFunctions() {
	}
	
	public static Value decode(List<Value> args) {
		logger.debug("CustomFunctions : perform() : decode : Input  : Args - {}", args);
		if (args.get(0).asBoolean()) {
			return args.get(1);
		}
		return args.get(2);
	}

	/**
	 * This function will look up for values provided in HBase
	 * 
	 * For boolInLookup() - accepts 2 parameters
	 * 
	 * @param0 - table name
	 * @param1 - Condition
	 * @return - boolean Value
	 * 
	 * @author Vivek Kasadwar
	 * 
	 * @return - True if Found , else False
	 */
	public static Value boolInLookup(String arg1, String arg2) {
		logger.debug("CustomFunctions : perform() : decode : Input  : Args - {} {}",arg1,arg2);
		String query = " SELECT COUNT(*) FROM " + arg1 + LookUpFunctionBso.getSubquery(arg2, arg1) + FIRST_ROW;
		logger.info(query);
		logger.debug("CustomFunctions : perform() : bool_in_lookup : {}", query);
		List<List<Object>> output = LookUpFunctionBso.executeHbaseQuery(query);
		logger.debug("CustomFunctions : perform() : bool_in_lookup : {}", output);
		if ((null != output && output.size() > 1) && (Integer.parseInt((String) output.get(1).get(0)) > 0)) {
				return new Value(true);
		}
		return new Value(false);
	}

	public static Value lookUpCheck(String arg1, String arg2, String arg3) {

		String query = SELECT + LookUpFunctionBso.getColumns(arg3) + FROM + arg1
				+ (arg2.equals("")?"":LookUpFunctionBso.getSubquery(arg2, arg1)) + FIRST_ROW;
		logger.debug(" << CustomFunctions : perform() : look_up_check : {}", query);
		logger.info(query);
		List<List<Object>> output = LookUpFunctionBso.executeHbaseQuery(query);
		logger.info("Output is {}", output);
		if (null != output && output.size() > 1) {
			logger.info("output : {}", output.get(1));
			if (null != output.get(1).get(0)) {
				return getDataSpecificValue(new Value(output.get(1).get(0)));
			}
		}
		return null;
	}

	public static Value lookUpMap(String arg1, String arg2, String arg3) {
		String query = SELECT + LookUpFunctionBso.getColumns(arg3) + FROM + arg1
				+ LookUpFunctionBso.getSubquery(arg2, arg1);
		logger.debug(" >> CustomFunctions : perform() : lookUpMap : {}", query);
		logger.info(query);
		List<List<Object>> output = LookUpFunctionBso.executeHbaseQuery(query);
		logger.info("lookUpMap() >> Output is {}", output);
		if (null != output) {
			return new Value(output.toString());
		} else {
			return null;
		}
	}

	public static Value lookup(String arg1, String arg2, String arg3, String arg4) {
		String query = SELECT + LookUpFunctionBso.getColumns(arg3) + FROM + arg1
				+ LookUpFunctionBso.getSubquery(arg2, arg1) + FIRST_ROW;
		logger.debug(" << CustomFunctions : perform() : lookup : {}", query);
		logger.info(query);
		logger.info("Query before execution is : {}", query);
		List<List<Object>> output = LookUpFunctionBso.executeHbaseQuery(query);
		if (null != output && output.size() > 1) {
			logger.info("output : {}", output.get(1).get(0));
			logger.info("Output after query  execution is : {}", output);
			if (null != output.get(1).get(0)) {
				return getDataSpecificValue(new Value(output.get(1).get(0)));
			}
		} else {
			return new Value(arg4);
		}
		return null;
	}

	public static Value nvl(Value arg1, Value arg2) {
		if (null != arg1 && !StringUtils.isEmpty(arg1.toString())) {
			return arg1;
		} else {
			return arg2;
		}
	}

	public static Value getDataSpecificValue(Value value) {
		logger.info("getDataSpecificValue() >> {}", value);

		if (!StringUtils.isBlank(null != value ? value.toString() : null)
				&& NumberUtils.isNumber(value.toString().trim())) {
			try {
				logger.debug("getDataSpecificValue() << {}", new Value(new BigDecimal(value.toString().trim())));
				return new Value(new BigDecimal(value.toString().trim()));
			} catch (Exception e) {
				logger.debug("getDataSpecificValue >> Error : {}", e.getMessage());
			}
		}
		return new Value(value);
	}
}
